package de.notepass.notaping.client.cli;

import de.notepass.notaping.core.shared.ResourceBundleUtils;

import java.util.ResourceBundle;

public enum CLI_RESULT_ERROR_CODE {
    NO_ERROR(0),
    ARGUMENT_ERROR(1),
    DEVICE_INIT_ERROR(2),
    DEVICE_OPERATION_ERROR(3),
    FILE_COLLECTION_ERROR(4),
    ;

    private int intCode;
    private String errorDescription;
    private static final ResourceBundle labels = ResourceBundle.getBundle("CliClientErrorCodeLabels");

    CLI_RESULT_ERROR_CODE(int intCode, String errorDescription) {
        this.intCode = intCode;
        this.errorDescription = errorDescription;
    }

    CLI_RESULT_ERROR_CODE(int intCode) {
        this.intCode = intCode;
        this.errorDescription = "CLI_RESULT_ERROR_CODE."+intCode;
    }

    public int getIntCode() {
        return intCode;
    }

    public String getErrorDescription() {
        return ResourceBundleUtils.getLabel(labels, errorDescription, false);
    }

    public String getLocalizedErrorDescription() {
        return ResourceBundleUtils.getLabel(labels, errorDescription, false);
    }
}
