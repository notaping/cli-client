package de.notepass.notaping.client.cli;

import de.notepass.notaping.core.access.jTapeAccess.SimpleTapeDriveUtils;
import de.notepass.notaping.core.access.jTapeAccess.impl.file.FileBasedTapeDrive;
import de.notepass.notaping.core.access.jTapeAccess.model.BlockSizeCorrectingTapeOutputStream;
import de.notepass.notaping.core.access.jTapeAccess.model.ExtendedTapeDrive;
import de.notepass.notaping.core.access.jTapeAccess.model.SimpleTapeDrive;
import de.notepass.notaping.core.client.base.JTapeAccessOperationMapper;
import de.notepass.notaping.core.fs.etfs.action.impl.NoopTapeSpecificOperationDispatcher;
import de.notepass.notaping.core.fs.etfs.action.model.FileSelector;
import de.notepass.notaping.core.fs.etfs.action.model.PathRemapper;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsReadDirectionBuilder;
import de.notepass.notaping.core.fs.etfs.data.listener.impl.EtfsNoopReadProgressListener;
import de.notepass.notaping.core.fs.etfs.data.listener.model.MultitapeAction;
import de.notepass.notaping.core.fs.etfs.data.model.section.TableOfContentSection;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;

import java.nio.file.*;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.regex.Pattern;

@CommandLine.Command(name = "restore", mixinStandardHelpOptions = true, version = "TODO", description = "Restore utility for reading tape archives")
public class NotapingRestoreClient implements Callable<Integer> {
    private String lastOut = "";
    private static final Logger LOGGER = LoggerFactory.getLogger(NotapingRestoreClient.class);

    public static void main(String[] args) {
        int exitCode = new CommandLine(new NotapingRestoreClient()).execute(args);
        System.exit(exitCode);
    }

    @CommandLine.Parameters(index = "0", description = "The folder to back up")
    private Path basedir;

    @CommandLine.Parameters(index = "1", description = "The file representing the tape drive")
    private String tapeDriveFile;

    @CommandLine.Option(names = {"--only-include"}, description = "If this option is present, only files matching the " +
            "given glob pattern(s) are added to the archive. If multiple pattern are given, only one needs to match for the file to be included. " +
            "Please note, that no other only-include/only-exclude option is allowed to be present if this option is present. Values are separated by ;")
    private String onlyIncludeGlob[];

    @CommandLine.Option(names = {"--only-exclude"}, description = "If this option is present, only files NOT matching the " +
            "given glob pattern(s) are added to the archive. If multiple pattern are given, only one needs to match for the file to be excluded. " +
            "Please note, that no other only-include/only-exclude option is allowed to be present if this option is present. Values are separated by ;")
    private String onlyExcludeGlob[];

    @CommandLine.Option(names = {"--only-include-regex"}, description = "If this option is present, only files matching the " +
            "given regex pattern(s) are added to the archive. If multiple pattern are given, only one needs to match for the file to be included. " +
            "Please note, that no other only-include/only-exclude option is allowed to be present if this option is present. Values are separated by ;")
    private String onlyIncludeRegex[];

    @CommandLine.Option(names = {"--only-exclude-regex"}, description = "If this option is present, only files NOT matching the " +
            "given regex pattern(s) are added to the archive. If multiple pattern are given, only one needs to match for the file to be excluded. " +
            "Please note, that no other only-include/only-exclude option is allowed to be present if this option is present. Values are separated by ;")
    private String onlyExcludeRegex[];

    @CommandLine.Option(names = {"-f", "--file"}, description = "Treat tape file parameter as reference to normal file instead of a tape device")
    private boolean fileMode;

    @Override
    public Integer call() {
        System.out.println("Checking parameters");
        int withDataCount = 0;
        PathMatcher[] matchers = new PathMatcher[0];
        boolean negateMatchers = false;

        if (onlyIncludeGlob != null && onlyIncludeGlob.length > 0) {
            withDataCount++;
            onlyIncludeGlob = onlyIncludeGlob[0].split(Pattern.quote(";"));
            negateMatchers = false;
            matchers = parseMatchers(onlyIncludeGlob, "glob");
        }

        if (onlyExcludeGlob != null && onlyExcludeGlob.length > 0) {
            withDataCount++;
            onlyExcludeGlob = onlyExcludeGlob[0].split(Pattern.quote(";"));
            negateMatchers = true;
            matchers = parseMatchers(onlyIncludeGlob, "glob");
        }

        if (onlyIncludeRegex != null && onlyIncludeRegex.length > 0) {
            withDataCount++;
            onlyIncludeRegex = onlyIncludeRegex[0].split(Pattern.quote(";"));
            negateMatchers = false;
            matchers = parseMatchers(onlyIncludeGlob, "regex");
        }

        if (onlyExcludeRegex != null && onlyExcludeRegex.length > 0) {
            withDataCount++;
            onlyExcludeRegex = onlyExcludeRegex[0].split(Pattern.quote(";"));
            negateMatchers = true;
            matchers = parseMatchers(onlyIncludeGlob, "regex");
        }

        if (withDataCount > 1) {
            System.out.println("Only one of --only-include, --only-exclude, --only-include-regex or --only-exclude-regex can be set!");
            return 1;
        }

        final SimpleTapeDrive tapeDrive;
        try {
            if (fileMode) {
                tapeDrive = FileBasedTapeDrive.createForFile(tapeDriveFile, FileBasedTapeDrive.STREAM_MODE.READ);
            } else {
                tapeDrive = SimpleTapeDriveUtils.getSimpleTapeDriveInstance(tapeDriveFile, false);
            }

            if (tapeDrive.getTapeInfo().getBlockSizeBytes() <= 0) {
                System.out.println("Detected tape with variable block size. This is unsupported. Will force usage of 64k block size.");
                tapeDrive.setMediumBlockSize(64 * 1024);
            }
        } catch (Exception e) {
            LOGGER.error("Error while trying to initialise tape drive for usage:", e);
            return CLI_RESULT_ERROR_CODE.DEVICE_INIT_ERROR.getIntCode();
        }

        final int[] fileIndex = {1};
        final long[] startTime = {0};

        PathMatcher[] finalMatchers = matchers;
        boolean finalNegateMatchers = negateMatchers;

        EtfsInstance etfs = EtfsReadDirectionBuilder
                .newInstance(new PathRemapper() {
                    @Override
                    public void prepareFileMapping(List<FileEntry> allFiles) {

                    }

                    @Override
                    public Path map(String normalizedPath) {
                        normalizedPath = normalizedPath.replaceFirst("^([A-Z]):\\\\", "/$1/").replace('\\', '/');
                        try {
                            return Paths.get(basedir.toString(), normalizedPath);
                        } catch (Exception e) {
                            if (! (e instanceof InvalidPathException)) {
                                LOGGER.error("Error while reading path:", e);
                            }
                            Path p = Paths.get(basedir.toString(),"./invalid_source_paths/", "./_ERROR_GETTING_FILE_NAME_"+UUID.randomUUID());
                            LOGGER.warn("File name on tape ("+normalizedPath+") contains characters not supported by this filesystem. File will be stored as "+p.toString(), e);
                            return p;
                        }
                    }
                })
                .withTapeSpecificOperationDispatcher(fileMode ? new NoopTapeSpecificOperationDispatcher() : new JTapeAccessOperationMapper(tapeDrive))
                .withFileSelector(new FileSelector() {
                    @Override
                    public void prepareFileSelection(List<FileEntry> allFiles) {

                    }

                    @Override
                    public boolean writeFile(FileEntry file) {
                        try {
                            boolean res = applyMatchers(Paths.get(file.getFilename()), finalMatchers, finalNegateMatchers);
                            return res; //Debugging help
                        } catch (Exception e) {
                            if (! (e instanceof InvalidPathException)) {
                                LOGGER.warn("Error while reading path. File will NOT be restored:", e);
                                e.printStackTrace();
                            }

                            LOGGER.warn("File name on tape ("+file.getFilename()+") contains characters not supported by this filesystem. File will NOT be restored", e);
                            return false;
                        }
                    }
                })
                .build();

        try {
            etfs.deserialize(tapeDrive.getInputStream(), new EtfsNoopReadProgressListener() {
                long numTocEntries = 0;

                @Override
                public void onStartReadTableOfContentSection(long numberOfEntries) {
                    print("Step 1 of 3: Fetching file metadata (read  header): 0 out of " + numberOfEntries + " files done");
                    numTocEntries = numberOfEntries;
                }

                @Override
                public void onStartReadTableOfContentsEntry() {
                    fileIndex[0]++;
                }

                @Override
                public void onEndReadTableOfContentsEntry(FileEntry entry) {
                    if (fileIndex[0] % 10 == 0)
                        print("Step 1 of 3: Fetching file metadata (read  header): " + fileIndex[0] + " out of " + numTocEntries + " files done");
                }

                @Override
                public void onEndReadTableOfContentSection(TableOfContentSection tableOfContentSection) {
                    fileIndex[0] = 1;
                    startTime[0] = System.currentTimeMillis();
                    print("Step 1 of 3: Fetching file metadata and create header: DONE");
                    print(null);
                }

                long cachedReportSize = -1;

                @Override
                public long getReportFileWriteProgressBytes() {
                    if (cachedReportSize > 0) {
                        return cachedReportSize;
                    }

                    try {
                        if (tapeDrive.getOutputStream() instanceof BlockSizeCorrectingTapeOutputStream) {
                            int bufferSize = ((BlockSizeCorrectingTapeOutputStream) tapeDrive.getOutputStream()).getBufferSize() / 2;
                            cachedReportSize = bufferSize;
                            return bufferSize;
                        }
                    } catch (Exception e) {
                        cachedReportSize = 64 * 1024 * 1024;
                        return cachedReportSize;
                    }

                    cachedReportSize = 64 * 1024 * 1024;
                    return cachedReportSize;
                }

            /*
            @Override
            public long getReportOverallFileReadProgressBytes() {
                return getReportFileWriteProgressBytes() * 8;
            }

             */

                @Override
                public void onFileProgress(Path target, long currentPosition, long fileSize) {
                    //System.out.println("File write progress for \"" + target + "\" (File " + fileIndex[0] + " of " + etfs.getTableOfContentSection().getFileEntries().size() + "): " + Math.round((currentPosition / Math.pow(1024, 2))) + "MiB of " + Math.round((fileSize / Math.pow(1024, 2))) + "MiB (" + (Math.round((currentPosition / (double) fileSize) * 100)) + "%)");
                }

            /*
            @Override
            public void onOverallFileProgress(long bytesWritten, long overallBytesToWrite) {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                    long timeTaken = System.currentTimeMillis() - startTime[0];
                    double bps = (bytesWritten) / (((double) timeTaken) / 1000);
                    double mbps = (bytesWritten / Math.pow(1024, 2)) / (((double) timeTaken) / 1000);
                    double gibWritten = (((double) bytesWritten) / Math.pow(1024, 3));
                    double gibTotal = (((double) overallBytesToWrite) / Math.pow(1024, 3));
                    double percentage = (((double) bytesWritten) / overallBytesToWrite) * 100;
                    long timeRemainingSeconds = (long) ((overallBytesToWrite - bytesWritten) / bps);
                    LocalDateTime eta = LocalDateTime
                            .ofInstant(
                                    Instant.ofEpochMilli(System.currentTimeMillis() + (timeRemainingSeconds * 1000)),
                                    TimeZone.getDefault().toZoneId()
                            );

                    print(String.format("Step 3 of 3 Writing file contents: Wrote %.2fGiB of %.2fGiB (%.2f%%), with about %dMiB/sec (ETA: %s)", gibWritten, gibTotal, percentage, Math.round(mbps), sdf.format(new Date(System.currentTimeMillis() + (timeRemainingSeconds * 1000)))));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

             */

                @Override
                public void onEndReadFileSection() {
                    long overallFileSize = etfs.getLogicalTapeInfo().getTableOfContentSection().getFileEntries().stream().map(FileEntry::getFileSize).reduce(Long::sum).orElse(0L);
                    //onOverallFileProgress(overallFileSize, overallFileSize);
                }

                @Override
                public void onStartReadFileSection(long entries) {
                    fileIndex[0] = 1;
                    print(String.format("Step 3 of 3 Reading file contents: Wrote %.2fGiB of %.2fGiB (%.2f%%), with about %dMiB/sec (ETA: %s)", (double) 0, (double) etfs.getLogicalTapeInfo().getTableOfContentSection().getFileEntries().stream().map(FileEntry::getFileSize).reduce(Long::sum).get() / Math.pow(1024, 3), (double) 0, Math.round(0), "N/A"));
                }

                @Override
                public void onStartReadFile(Path target, long fileSize) {
                    print(String.format("Step 3 of 3: Reading file %d of %d: %s", fileIndex[0], etfs.getLogicalTapeInfo().getTableOfContentSection().getFileEntries().size(), target.getFileName().toString()));
                    fileIndex[0]++;
                }

                @Override
                public void onEndReadFile(Path target, long fileSize) {
                    long timeTaken = System.currentTimeMillis() - startTime[0];
                    double mbps = (fileSize / Math.pow(1024, 2)) / (timeTaken / 1000);
                    //if (fileIndex[0] % 500 == 0)
                    //    System.out.println("Ending write for \"" + target + "\" (File " + fileIndex[0] + " of " + etfs.getTableOfContentSection().getFileEntries().size() + ") in " + Math.round(timeTaken / 1000) + " seconds with " + Math.round(mbps) + " MB/s");
                    fileIndex[0]++;
                }

                @Override
                public MultitapeAction multitapeTapeChangeNeeded(String volumeLabel, UUID volumeId, int currentTapeIndex, int nextTapeIndex) {
                    try {
                        if (tapeDrive instanceof ExtendedTapeDrive) {
                            System.out.println("Tape end. Ejecting");
                            if (1 == 1)
                                return MultitapeAction.ABORT;
                            ((ExtendedTapeDrive) tapeDrive).ejectTape();
                        } else {
                            System.out.println("Tape full. Please eject and insert new one!");
                        }

                        System.out.println("Waiting for ejection to finish");
                        while (tapeDrive.isMediaPresent()) {
                            Thread.sleep(1000);
                        }

                        System.out.println("Please insert next tape");
                        while (!tapeDrive.isMediaPresent()) {
                            Thread.sleep(1000);
                        }

                        startTime[0] = System.currentTimeMillis();

                        return MultitapeAction.TAPE_CHANGED;
                    } catch (Exception e) {
                        return MultitapeAction.ABORT;
                    }
                }
            });
        } catch (Exception e) {
            LOGGER.error("Error while restoring files:", e);
            LOGGER.debug("Now might also be the right time to remind you that this software comes \"WITHOUT WARRANTY OF ANY KIND\", according to the licence");
            return CLI_RESULT_ERROR_CODE.DEVICE_OPERATION_ERROR.getIntCode();
        }

        return CLI_RESULT_ERROR_CODE.NO_ERROR.getIntCode();
    }

    protected static boolean applyMatchers(Path p, PathMatcher[] matchers, boolean invert) {
        if (matchers == null || matchers.length == 0) {
            return true;
        }

        if (invert) {
            for (PathMatcher m : matchers) {
                if (m.matches(p)) {
                    return false;
                }
            }

            return true;
        } else {
            for (PathMatcher m : matchers) {
                if (m.matches(p)) {
                    return true;
                }
            }

            return false;
        }
    }

    protected static PathMatcher[] parseMatchers(String[] pattern, String syntax) {
        PathMatcher[] matchers = new PathMatcher[pattern.length];
        for (int i = 0; i < matchers.length; i++) {
            matchers[i] = FileSystems.getDefault().getPathMatcher(syntax+":"+pattern[i]);
        }

        return matchers;
    }

    private void print(String msg) {
        if (msg == null) {
            lastOut = "";
            System.out.println();
            return;
        }

        int minSize = lastOut.length();
        StringBuilder msgBuilder = new StringBuilder(msg);
        while (msgBuilder.length() < minSize) {
            msgBuilder.append(' ');
        }
        msg = msgBuilder.toString();

        System.out.print(msg+'\r');
        lastOut = msg;
    }
}
