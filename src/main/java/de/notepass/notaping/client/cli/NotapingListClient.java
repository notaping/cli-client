package de.notepass.notaping.client.cli;

import de.notepass.notaping.core.access.jTapeAccess.SimpleTapeDriveUtils;
import de.notepass.notaping.core.access.jTapeAccess.model.SimpleTapeDrive;
import de.notepass.notaping.core.client.base.JTapeAccessOperationMapper;
import de.notepass.notaping.core.fs.etfs.action.impl.AcceptAllFileSelector;
import de.notepass.notaping.core.fs.etfs.action.impl.NoopTapeSpecificOperationDispatcher;
import de.notepass.notaping.core.fs.etfs.action.model.PathRemapper;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsReadDirectionBuilder;
import de.notepass.notaping.core.fs.etfs.data.listener.impl.EtfsNoopReadProgressListener;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class NotapingListClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(NotapingListClient.class);

    public static void main(String[] args) {
        if (args.length <= 0) {
            System.out.println("Missing first parameter: Tape device path");
            return;
        }

        if (args[0].equals("-h") || args[0].equals("--help")) {
            System.out.println("list <Path to tape drive>");
            return;
        }

        String tapeDriveFile = args[0];

        SimpleTapeDrive tapeDrive;
        try {
            tapeDrive = SimpleTapeDriveUtils.getSimpleTapeDriveInstance(tapeDriveFile, false);
            if (tapeDrive.getTapeInfo().getBlockSizeBytes() <= 0) {
                LOGGER.warn("Detected tape with variable block size. This is unsupported. Will force usage of 64k block size.");
                tapeDrive.setMediumBlockSize(64 * 1024);
            }

            //if (!tapeDrive.isMediaPresent()) {
            //    System.out.println("Please insert a tape into drive " + tapeDriveFile);
            //    return;
            //}

            //if (!tapeDrive.isReadable()) {
            //    System.out.println("Tape in drive " + tapeDriveFile + " is not readable");
            //    return;
            //}
        } catch (Exception e) {
            System.err.println("Could not initialize tape device: "+e.getMessage());
            e.printStackTrace();
            return;
        }

        EtfsInstance etfs = EtfsReadDirectionBuilder
                .newInstance(new PathRemapper() {
                    Path p = Paths.get(".");

                    @Override
                    public void prepareFileMapping(List<FileEntry> allFiles) {

                    }

                    @Override
                    public Path map(String normalizedPath) {
                        return p;
                    }
                })
                .withFileSelector(new AcceptAllFileSelector())
                .withTapeSpecificOperationDispatcher(new JTapeAccessOperationMapper(tapeDrive))
                .build();

        EtfsReadProgressCallback callback = new EtfsNoopReadProgressListener();

        try {
            InputStream is = tapeDrive.getInputStream();
            is = etfs.getLogicalTapeInfo().getHeaderSection().deserialize(is, -1, true, etfs, callback);
            int version = etfs.getLogicalTapeInfo().getHeaderSection().getVersion();
            is = etfs.getLogicalTapeInfo().getPluginConfigSection().deserialize(is, version, true, etfs, callback);
            is = etfs.getLogicalTapeInfo().getTableOfContentSection().deserialize(is, version, true, etfs, callback);
        } catch (Exception e) {
            System.err.println("Could not read file list from tape: "+e.getMessage());
            e.printStackTrace();
            return;
        }

        System.out.println("====================================================");
        System.out.println("Tape info:");
        System.out.println("Volume-ID: "+etfs.getLogicalTapeInfo().getHeaderSection().getVolumeId());
        System.out.println("Label: "+etfs.getLogicalTapeInfo().getHeaderSection().getLabel());
        System.out.println("Multitape index: "+etfs.getLogicalTapeInfo().getHeaderSection().getMultitapeIndex());
        System.out.println("====================================================");

        System.out.println("File list:");
        for (FileEntry fe : etfs.getLogicalTapeInfo().getTableOfContentSection().getFileEntries()) {
            System.out.printf("%s (%s)%n", fe.getFilename(), formatFileSize(fe.getFileSize()));
        }
    }

    private static final String[] units = {"bytes", "kib", "mib", "gib", "tib"};
    private static final float fallback = 1.2F;
    private static final double nextUnit = 1024;
    private static final String fileSizeFormatter = "%.2f %s";

    public static String formatFileSize(long fileSizeBytes) {
        double fileSize = fileSizeBytes;

        for (int i = 1; i < units.length; i++) {
            fileSize = fileSize / nextUnit;

            if (fileSize < fallback) {
                return String.format(fileSizeFormatter, (fileSize * nextUnit), units[i-1]);
            }
        }

        return String.format(fileSizeFormatter, (fileSize), units[units.length - 1]);
    }
}
