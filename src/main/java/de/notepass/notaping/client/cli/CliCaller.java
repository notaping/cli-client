package de.notepass.notaping.client.cli;

public class CliCaller {
    public static void main(String[] args) {
        if (args.length <= 1) {
            System.out.println("First parameter must be backup, restore or list. More help is available with <subcommand> -h (e.g. backup -h)");
            return;
        }

        String[] executableArgs = new String[args.length - 1];
        System.arraycopy(args, 1, executableArgs, 0, executableArgs.length);
        if (args[0].equals("backup")) {
            NotapingBackupClient.main(executableArgs);
        } else if (args[0].equals("restore")) {
            NotapingRestoreClient.main(executableArgs);
        } else if (args[0].equals("list")) {
            NotapingListClient.main(executableArgs);
        } else if (args[0].equals("structInfo")) {
            NotapingDataStructDebuggerClient.main(executableArgs);
        } else {
            System.out.println("First parameter must be backup, restore or list. More help is available with restore -h or backup -h");
            return;
        }
    }
}
