package de.notepass.notaping.client.cli;

import de.notepass.notaping.core.access.jTapeAccess.SimpleTapeDriveUtils;
import de.notepass.notaping.core.access.jTapeAccess.impl.file.FileBasedTapeDrive;
import de.notepass.notaping.core.access.jTapeAccess.model.BlockSizeCorrectingTapeInputStream;
import de.notepass.notaping.core.access.jTapeAccess.model.SimpleTapeDrive;
import de.notepass.notaping.core.access.jTapeAccess.model.TapeInputStream;
import de.notepass.notaping.core.client.base.JTapeAccessOperationMapper;
import de.notepass.notaping.core.fs.etfs.action.impl.AcceptAllFileSelector;
import de.notepass.notaping.core.fs.etfs.action.impl.NoopTapeSpecificOperationDispatcher;
import de.notepass.notaping.core.fs.etfs.action.model.PathRemapper;
import de.notepass.notaping.core.fs.etfs.data.listener.impl.EtfsNoopReadProgressListener;
import de.notepass.notaping.core.fs.etfs.data.listener.model.EtfsReadProgressCallback;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsReadDirectionBuilder;
import de.notepass.notaping.core.fs.etfs.plugin.EtfsPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Pattern;

public class NotapingDataStructDebuggerClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(NotapingDataStructDebuggerClient.class);

    public static void main(String[] args) {
        if (args.length <= 0) {
            System.out.println("Missing first parameter: Tape device path");
            return;
        }

        if (args[0].equals("-h") || args[0].equals("--help")) {
            System.out.println("structInfo <Path to tape drive> [-f]");
            System.out.println("If -f is passed as the last parameter, the input will be handled as a file and not a tape device");
            return;
        }

        boolean fileMode = false;
        if (args.length > 1) {
            fileMode = "-f".equals(args[1]);
        }

        String tapeDriveFile = args[0];

        SimpleTapeDrive tapeDrive;
        try {
            if (fileMode) {
                tapeDrive = FileBasedTapeDrive.createForFile(tapeDriveFile, FileBasedTapeDrive.STREAM_MODE.READ);
            } else {
                tapeDrive = SimpleTapeDriveUtils.getSimpleTapeDriveInstance(tapeDriveFile, false);
            }
            if (tapeDrive.getTapeInfo().getBlockSizeBytes() <= 0 && tapeDrive.isWritable()) {
                LOGGER.warn("Detected tape with variable block size. This is unsupported. Will force usage of 64k block size.");
                tapeDrive.setMediumBlockSize(64 * 1024);
            }

            //if (!tapeDrive.isMediaPresent()) {
            //    System.out.println("Please insert a tape into drive " + tapeDriveFile);
            //    return;
            //}

            //if (!tapeDrive.isReadable()) {
            //    System.out.println("Tape in drive " + tapeDriveFile + " is not readable");
            //    return;
            //}
        } catch (Exception e) {
            System.err.println("Could not initialize tape device: "+e.getMessage());
            e.printStackTrace();
            return;
        }

        EtfsInstance etfs = EtfsReadDirectionBuilder
                .newInstance(new PathRemapper() {
                    Path p = Paths.get(".");

                    @Override
                    public void prepareFileMapping(List<FileEntry> allFiles) {

                    }

                    @Override
                    public Path map(String normalizedPath) {
                        return p;
                    }
                })
                .withFileSelector(new AcceptAllFileSelector())
                .withTapeSpecificOperationDispatcher(fileMode ? new NoopTapeSpecificOperationDispatcher() : new JTapeAccessOperationMapper(tapeDrive))
                .build();

        EtfsReadProgressCallback callback = new EtfsNoopReadProgressListener();

        BlockSizeCorrectingTapeInputStream is;
        try {
            is = (BlockSizeCorrectingTapeInputStream) tapeDrive.getInputStream();
            is = (BlockSizeCorrectingTapeInputStream) etfs.getLogicalTapeInfo().getHeaderSection().deserialize(is, -1, true, etfs, callback);
            int version = etfs.getLogicalTapeInfo().getHeaderSection().getVersion();
            is = (BlockSizeCorrectingTapeInputStream) etfs.getLogicalTapeInfo().getPluginConfigSection().deserialize(is, version, true, etfs, callback);
            is = (BlockSizeCorrectingTapeInputStream) etfs.getLogicalTapeInfo().getTableOfContentSection().deserialize(is, version, true, etfs, callback);
        } catch (Exception e) {
            System.err.println("Error while reading tape header: "+e.getMessage());
            e.printStackTrace();
            return;
        }

        int blockBufferPosition;
        long blockAfterHeaderRead;
        byte[] blockBuffer;
        long numBlocksBuffered;
        long blockSizeBytes;
        long byteStartOffset;
        try {
            Field blockBufferPositionField = BlockSizeCorrectingTapeInputStream.class.getDeclaredField("blockBufferPosition");
            Field blockBufferField = BlockSizeCorrectingTapeInputStream.class.getDeclaredField("blockBuffer");

            blockBufferPositionField.setAccessible(true);
            blockBufferField.setAccessible(true);

            blockBufferPosition = blockBufferPositionField.getInt(is);
            blockBuffer = (byte[]) blockBufferField.get(is);

            blockAfterHeaderRead = tapeDrive.getTapeBlockPosition();
            blockSizeBytes = tapeDrive.getTapeInfo().getBlockSizeBytes();
            numBlocksBuffered = (long) Math.ceil((double) blockBuffer.length / (double) blockSizeBytes);
            byteStartOffset = (blockBufferPosition + (blockAfterHeaderRead - 1) * blockSizeBytes);
        } catch (Exception e) {
            System.err.println("Error while accessing block info: "+e.getMessage());
            e.printStackTrace();
            return;
        }

        System.out.println("==================== GENERAL MEDIA INFO ================================");
        System.out.println("Tape info:");
        System.out.println("Volume-ID: "+etfs.getLogicalTapeInfo().getHeaderSection().getVolumeId());
        System.out.println("Label: "+etfs.getLogicalTapeInfo().getHeaderSection().getLabel());
        System.out.println("Multitape index: "+etfs.getLogicalTapeInfo().getHeaderSection().getMultitapeIndex());
        System.out.println();
        System.out.println("Tape content info:");
        System.out.println("Block size: "+blockSizeBytes);
        System.out.println("Current block position: "+blockAfterHeaderRead);
        System.out.println("Number of blocks in buffer: "+numBlocksBuffered);
        System.out.println("Byte position in block buffer: "+blockBufferPosition+" (In buffered block "+((long) Math.ceil((double)blockBufferPosition / (double)blockSizeBytes))+")");
        System.out.println("Number of plugins (according to registry): "+etfs.getPluginRegistry().getActivePlugins().length);
        System.out.println("Number of files: "+etfs.getLogicalTapeInfo().getTableOfContentSection().getFileEntries().size()+" (Overall size: "+etfs.getLogicalTapeInfo().getTableOfContentSection().getFileEntries().stream().map(FileEntry::getFileSize).reduce(Long::sum).get()+")");
        System.out.println("Offset for file start position calculation: "+(blockAfterHeaderRead - 1)+" blocks and "+blockBufferPosition+" bytes (= "+byteStartOffset+" bytes)");
        System.out.println("========================================================================"+System.lineSeparator());

        System.out.println("==================== PLUGIN INFO ================================");
        System.out.println("List of plugins according to registry:");
        System.out.println("Format: <Internal ID>, <Global unique plugin ID>, <Plugin version>");
        for (EtfsPlugin plugin : etfs.getPluginRegistry().getActivePlugins()) {
            System.out.printf("%d, %s, %d%n", plugin.getInternalId(), plugin.getID(), plugin.getVersion());
        }
        System.out.println("================================================================="+System.lineSeparator());


        long feSizesTillHere = 0;
        System.out.println("==================== FILE INFO ================================");
        System.out.println("File list (according to TOC):");
        System.out.println("Format: <File path>, <File size in bytes>, <File entry size in bytes>, <Block containing first byte of file>, <Number of blocks spanned by file entry>, <Byte offset to file entry start in first block>, <Byte offset to first byte of file>, <command to dump file entry>, <command to dump file content>");
        long fileBlockNum; // Block number containing the first byte of file entry data
        long fileBlockByteOffset; // Number of bytes in the block _BEFORE_ the first byte of the file entry (so, skip fileBlockByteOffset and the next byte read is the first byte of the file entry)
        long fileBlockSpan; // Number of bock which need to be read to get the complete file entry content (After fileBlockNum - 1)
        String fileName;
        for (FileEntry fe : etfs.getLogicalTapeInfo().getTableOfContentSection().getFileEntries()) {
            fileBlockNum = blockAfterHeaderRead + (long) Math.ceil((double)feSizesTillHere / (double)blockSizeBytes);
            fileBlockByteOffset = (byteStartOffset + feSizesTillHere) % blockSizeBytes;
            fileBlockSpan = ((long) Math.ceil((double)(byteStartOffset + feSizesTillHere + fe.getOverallFileContentEntrySize())/(double)blockSizeBytes) - fileBlockNum) + 1;
            try {
                fileName = Paths.get(fe.getFilename()).getFileName().toString();
            } catch (Exception e) {
                // Sometimes chars cannot be mapped if the creating system used the wrong charset
                // So fall back and try to extract the file name manually
                String fn = fe.getFilename();
                fn = fn.replace('\\', '/');
                String[] parts = fn.split(Pattern.quote("/"));
                if (parts.length > 0) {
                    fileName = parts[parts.length - 1];
                } else {
                    fileName = "_ERROR_GETTING_FILE_NAME_"+ UUID.randomUUID();
                }
            }
            System.out.printf("%s, %d, %d, %d, %d, %d, %d, %s, %s%n",
                    fe.getFilename(),
                    fe.getFileSize(),
                    fe.getOverallFileContentEntrySize(),
                    fileBlockNum,
                    fileBlockSpan,
                    fileBlockByteOffset,
                    byteStartOffset + feSizesTillHere,
                    String.format(
                            "mt -f %s seek %d && (dd if=%s bs=%d count=%d | head -c %d | tail -c %s > %s.fe)",
                            tapeDriveFile,
                            fileBlockNum - 1,
                            tapeDriveFile,
                            blockSizeBytes,
                            fileBlockSpan,
                            fileBlockByteOffset + fe.getOverallFileContentEntrySize(),
                            fe.getOverallFileContentEntrySize(),
                            fileName
                    ),
                    String.format(
                            "mt -f %s seek %d && (dd if=%s bs=%d count=%d | head -c %d | tail -c %s > %s)",
                            tapeDriveFile,
                            fileBlockNum - 1,
                            tapeDriveFile,
                            blockSizeBytes,
                            fileBlockSpan,
                            fileBlockByteOffset + fe.getOverallFileContentEntrySize(),
                            fe.getFileSize(),
                            fileName
                    )
            );
            feSizesTillHere += fe.getOverallFileContentEntrySize();
        }
        System.out.println("==============================================================="+System.lineSeparator());

        /*
        try {
            tapeDrive.setTapeBlockPosition(0);
        } catch (Exception e) {
            System.err.println("Could not rewind tape: "+e.getMessage());
            e.printStackTrace();
            return;
        }
         */
    }

    private static final String[] units = {"bytes", "kib", "mib", "gib", "tib"};
    private static final float fallback = 1.2F;
    private static final double nextUnit = 1024;
    private static final String fileSizeFormatter = "%.2f %s";

    public static String formatFileSize(long fileSizeBytes) {
        double fileSize = fileSizeBytes;

        for (int i = 1; i < units.length; i++) {
            fileSize = fileSize / nextUnit;

            if (fileSize < fallback) {
                return String.format(fileSizeFormatter, (fileSize * nextUnit), units[i-1]);
            }
        }

        return String.format(fileSizeFormatter, (fileSize), units[units.length - 1]);
    }
}
