package de.notepass.notaping.client.cli;


import de.notepass.notaping.core.access.jTapeAccess.SimpleTapeDriveUtils;
import de.notepass.notaping.core.access.jTapeAccess.impl.file.FileBasedTapeDrive;
import de.notepass.notaping.core.access.jTapeAccess.impl.stream.StreamBasedTapeDrive;
import de.notepass.notaping.core.access.jTapeAccess.model.*;
import de.notepass.notaping.core.client.base.JTapeAccessOperationMapper;
import de.notepass.notaping.core.fs.etfs.action.impl.NoopTapeSpecificOperationDispatcher;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsInstance;
import de.notepass.notaping.core.fs.etfs.data.runtime.EtfsWriteDirectionBuilder;
import de.notepass.notaping.core.fs.etfs.data.listener.impl.EtfsNoopWriteProgressListener;
import de.notepass.notaping.core.fs.etfs.data.listener.model.MultitapeAction;
import de.notepass.notaping.core.fs.etfs.data.model.section.TableOfContentSection;
import de.notepass.notaping.core.fs.etfs.data.model.struct.FileEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.regex.Pattern;

@CommandLine.Command(name = "backup", mixinStandardHelpOptions = true, version = "TODO", description = "Backup utility for writing tape archives")
public class NotapingBackupClient implements Callable<Integer> {
    private String lastOut = "";
    private static final Logger LOGGER = LoggerFactory.getLogger(NotapingBackupClient.class);

    public static void main(String[] args) {
        int exitCode = new CommandLine(new NotapingBackupClient()).execute(args);
        System.exit(exitCode);
    }

    @CommandLine.Parameters(index = "0", description = "The folder to back up")
    private Path basedir;

    @CommandLine.Parameters(index = "1", description = "The file representing the tape drive")
    private String tapeDriveFile;

    @CommandLine.Option(names = {"--only-include"}, description = "If this option is present, only files matching the " +
            "given glob pattern(s) are added to the archive. If multiple pattern are given, only one needs to match for the file to be included. " +
            "Please note, that no other only-include/only-exclude option is allowed to be present if this option is present. Values are separated by ;")
    private String onlyIncludeGlob[];

    @CommandLine.Option(names = {"--only-exclude"}, description = "If this option is present, only files NOT matching the " +
            "given glob pattern(s) are added to the archive. If multiple pattern are given, only one needs to match for the file to be excluded. " +
            "Please note, that no other only-include/only-exclude option is allowed to be present if this option is present. Values are separated by ;")
    private String onlyExcludeGlob[];

    @CommandLine.Option(names = {"--only-include-regex"}, description = "If this option is present, only files matching the " +
            "given regex pattern(s) are added to the archive. If multiple pattern are given, only one needs to match for the file to be included. " +
            "Please note, that no other only-include/only-exclude option is allowed to be present if this option is present. Values are separated by ;")
    private String onlyIncludeRegex[];

    @CommandLine.Option(names = {"--only-exclude-regex"}, description = "If this option is present, only files NOT matching the " +
            "given regex pattern(s) are added to the archive. If multiple pattern are given, only one needs to match for the file to be excluded. " +
            "Please note, that no other only-include/only-exclude option is allowed to be present if this option is present. Values are separated by ;")
    private String onlyExcludeRegex[];

    @CommandLine.Option(names = {"--append"}, description = "If this option is present, the tape will be forwarded until" +
            "the end-of-data marker before writing")
    private boolean appendToEnd;

    @CommandLine.Option(names = {"--overwrite"}, description = "If this option is present, the tape will be written starting at the beginning. This option MUST BE set if --append is not set!")
    private boolean overwrite;

    @CommandLine.Option(names = {"--enable-compression", "-c"}, description = "Enables tape-drive compression (enabled by default)", defaultValue = "true")
    private boolean enableCompression;

    @CommandLine.Option(names = {"-f", "--file"}, description = "Treat tape file parameter as reference to normal file instead of a tape device")
    private boolean fileMode;

    @Override
    public Integer call() {
        System.out.println("Checking parameters");
        int withDataCount = 0;
        PathMatcher[] matchers = new PathMatcher[0];
        boolean negateMatchers = false;

        if (onlyIncludeGlob != null && onlyIncludeGlob.length > 0) {
            withDataCount++;
            onlyIncludeGlob = onlyIncludeGlob[0].split(Pattern.quote(";"));
            negateMatchers = false;
            matchers = parseMatchers(onlyIncludeGlob, "glob");
        }

        if (onlyExcludeGlob != null && onlyExcludeGlob.length > 0) {
            withDataCount++;
            onlyExcludeGlob = onlyExcludeGlob[0].split(Pattern.quote(";"));
            negateMatchers = true;
            matchers = parseMatchers(onlyIncludeGlob, "glob");
        }

        if (onlyIncludeRegex != null && onlyIncludeRegex.length > 0) {
            withDataCount++;
            onlyIncludeRegex = onlyIncludeRegex[0].split(Pattern.quote(";"));
            negateMatchers = false;
            matchers = parseMatchers(onlyIncludeGlob, "regex");
        }

        if (onlyExcludeRegex != null && onlyExcludeRegex.length > 0) {
            withDataCount++;
            onlyExcludeRegex = onlyExcludeRegex[0].split(Pattern.quote(";"));
            negateMatchers = true;
            matchers = parseMatchers(onlyIncludeGlob, "regex");
        }

        if (withDataCount > 1) {
            System.out.println("Only one of --only-include, --only-exclude, --only-include-regex or --only-exclude-regex can be set!");
            return CLI_RESULT_ERROR_CODE.ARGUMENT_ERROR.getIntCode();
        }

        if (overwrite == false && appendToEnd == false && !fileMode) {
            System.out.println("If data is not appended, the overwrite flag MUST be set to true. This is done to avoid accidental overwriting.");
            return CLI_RESULT_ERROR_CODE.ARGUMENT_ERROR.getIntCode();
        }

        final SimpleTapeDrive tapeDrive;
        try {
            if (fileMode) {
                tapeDrive = FileBasedTapeDrive.createForFile(tapeDriveFile, StreamBasedTapeDrive.STREAM_MODE.WRITE);
            } else {
                tapeDrive = SimpleTapeDriveUtils.getSimpleTapeDriveInstance(tapeDriveFile, false);
            }

            if (tapeDrive.getTapeInfo().getBlockSizeBytes() <= 0) {
                System.out.println("Detected tape with variable block size. This is unsupported. Will force usage of 64k block size.");
                tapeDrive.setMediumBlockSize(64 * 1024);
            }
        } catch (Exception e) {
            LOGGER.error("Error while trying to initialise tape drive for usage:", e);
            return CLI_RESULT_ERROR_CODE.DEVICE_INIT_ERROR.getIntCode();
        }

        System.out.println("Collecting files");
        List<Path> files = new ArrayList<>();

        PathMatcher[] finalMatchers = matchers;
        boolean finalNegateMatchers = negateMatchers;

        try {
            Files.walkFileTree(
                    basedir,
                    new HashSet<FileVisitOption>(),
                    Integer.MAX_VALUE, new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                                throws IOException {
                            if (Files.isRegularFile(file) && applyMatchers(file, finalMatchers, finalNegateMatchers)) {
                                files.add(file);
                            }

                            if (files.size() % 100 == 0 && files.size() > 0) {
                                print("Discovered " + files.size() + " files so far");
                            }

                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult visitFileFailed(Path file, IOException e)
                                throws IOException {
                            return FileVisitResult.SKIP_SUBTREE;
                        }

                        @Override
                        public FileVisitResult preVisitDirectory(Path dir,
                                                                 BasicFileAttributes attrs)
                                throws IOException {
                        /*
                        if (files.stream().anyMatch(f -> f.getParent().equals(dir))) {
                            // Already visited directory
                            return FileVisitResult.SKIP_SUBTREE;
                        } else {
                            // New directory
                            return FileVisitResult.CONTINUE;
                        }
                         */

                            return FileVisitResult.CONTINUE;
                        }
                    }
            );
        } catch (Exception e) {
            LOGGER.error("Error while collecting files for backup:", e);
            return CLI_RESULT_ERROR_CODE.FILE_COLLECTION_ERROR.getIntCode();
        }

        print(null);
        System.out.println("Found "+files.size()+" files");

        try {
            if (enableCompression) {
                System.out.println("Enabling CRC and compression features");
                tapeDrive.enableDriveCapability(DriveCapabilities.COMPRESSION, DriveCapabilities.ECC);
            } else {
                System.out.println("Enabling CRC feature");
                tapeDrive.enableDriveCapability(DriveCapabilities.ECC);
            }
        } catch (Exception e) {
            LOGGER.error("Error while enabling CRC/Compression feature(s):", e);
            return CLI_RESULT_ERROR_CODE.DEVICE_OPERATION_ERROR.getIntCode();
        }

        if (appendToEnd) {
            System.out.println("Forwarding tape to the end of data");
            try {
                tapeDrive.setTapeBlockPosition(Long.MAX_VALUE);
            } catch (TapeException e) {
                // End of data may be thrown here, if the native bindings implement it. So, ignore it
                if (e.getErrorCode() != TapeException.ErrorCode.END_OF_DATA_REACHED) {
                    // If the error is not a END_OF_DATA_REACHED error, rethrow
                    LOGGER.error("Error while forwarding tape to end of data marker:", e);
                    return CLI_RESULT_ERROR_CODE.DEVICE_OPERATION_ERROR.getIntCode();
                }
            }
        }

        if (overwrite && !fileMode) {
            System.out.println("Setting tape to start");
            System.out.println("******************************************************");
            System.out.println("* WARNING: ALL DATA ON THIS TAPE WILL BE OVERWRITTEN *");
            System.out.println("*            THIS ACTION CANNOT BE UNDONE            *");
            System.out.println("******************************************************");
            try {
                tapeDrive.setTapeBlockPosition(0);
            } catch (Exception e) {
                LOGGER.error("Error while reversing tape to the beginning:", e);
                return CLI_RESULT_ERROR_CODE.DEVICE_OPERATION_ERROR.getIntCode();
            }
        }

        EtfsInstance etfs = EtfsWriteDirectionBuilder
                .newInstance()
                .withTapeSpecificOperationDispatcher(fileMode ? new NoopTapeSpecificOperationDispatcher() : new JTapeAccessOperationMapper(tapeDrive))
                .withLabel("Test tape")
                .addFiles(files)
                .build();

        final int[] fileIndex = {1};
        final long[] startTime = {0};

        System.out.println("Starting to write files");
        // TODO: [High] Output for step 2: Plugin info
        try {
            etfs.serialize(tapeDrive.getOutputStream(), new EtfsNoopWriteProgressListener() {
                private final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");

                @Override
                public void onStartWriteTableOfContentSection(TableOfContentSection tableOfContentSection) {
                    print("Step 1 of 3: Fetching file metadata and create header: 0 out of " + files.size() + " files done");
                }

                @Override
                public void onStartWriteTableOfContentsEntry(FileEntry entry) {
                    fileIndex[0]++;
                }

                @Override
                public void onEndWriteTableOfContentsEntry(FileEntry entry) {
                    if (fileIndex[0] % 10 == 0)
                        print("Step 1 of 2: Fetching file metadata and create header: " + fileIndex[0] + " out of " + files.size() + " files done");
                }

                @Override
                public void onEndWriteTableOfContentSection(TableOfContentSection tableOfContentSection) {
                    fileIndex[0] = 1;
                    startTime[0] = System.currentTimeMillis();
                    print("Step 1 of 2: Fetching file metadata and create header: DONE");
                    print(null);
                }

                long cachedReportSize = -1;

                @Override
                public long getReportFileWriteProgressBytes() {
                    if (cachedReportSize > 0) {
                        return cachedReportSize;
                    }

                    try {
                        if (tapeDrive.getOutputStream() instanceof BlockSizeCorrectingTapeOutputStream) {
                            int bufferSize = ((BlockSizeCorrectingTapeOutputStream) tapeDrive.getOutputStream()).getBufferSize() / 2;
                            cachedReportSize = bufferSize;
                            return bufferSize;
                        }
                    } catch (Exception e) {
                        cachedReportSize = 64 * 1024 * 1024;
                        return cachedReportSize;
                    }

                    cachedReportSize = 64 * 1024 * 1024;
                    return cachedReportSize;
                }

                @Override
                public long getReportOverallFileWriteProgressBytes() {
                    return getReportFileWriteProgressBytes() * 8;
                }

                @Override
                public void onFileProgress(Path target, long currentPosition, long fileSize) {
                    //System.out.println("File write progress for \"" + target + "\" (File " + fileIndex[0] + " of " + etfs.getTableOfContentSection().getFileEntries().size() + "): " + Math.round((currentPosition / Math.pow(1024, 2))) + "MiB of " + Math.round((fileSize / Math.pow(1024, 2))) + "MiB (" + (Math.round((currentPosition / (double) fileSize) * 100)) + "%)");
                }

                @Override
                public void onOverallFileProgress(long bytesWritten, long overallBytesToWrite) {
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                        long timeTaken = System.currentTimeMillis() - startTime[0];
                        double bps = (bytesWritten) / (((double) timeTaken) / 1000);
                        double mbps = (bytesWritten / Math.pow(1024, 2)) / (((double) timeTaken) / 1000);
                        double gibWritten = (((double) bytesWritten) / Math.pow(1024, 3));
                        double gibTotal = (((double) overallBytesToWrite) / Math.pow(1024, 3));
                        double percentage = (((double) bytesWritten) / overallBytesToWrite) * 100;
                        long timeRemainingSeconds = (long) ((overallBytesToWrite - bytesWritten) / bps);
                        LocalDateTime eta = LocalDateTime
                                .ofInstant(
                                        Instant.ofEpochMilli(System.currentTimeMillis() + (timeRemainingSeconds * 1000)),
                                        TimeZone.getDefault().toZoneId()
                                );

                        print(String.format("Step 2 of 2 Writing file contents: Wrote %.2fGiB of %.2fGiB (%.2f%%), with about %dMiB/sec (ETA: %s)", gibWritten, gibTotal, percentage, Math.round(mbps), sdf.format(new Date(System.currentTimeMillis() + (timeRemainingSeconds * 1000)))));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onEndWriteFileSection() {
                    long overallFileSize = etfs.getLogicalTapeInfo().getTableOfContentSection().getFileEntries().stream().map(FileEntry::getFileSize).reduce(Long::sum).orElse(0L);
                    onOverallFileProgress(overallFileSize, overallFileSize);
                }

                @Override
                public void onStartWriteFileSection(long entries) {
                    print(String.format("Step 2 of 2 Writing file contents: Wrote %.2fGiB of %.2fGiB (%.2f%%), with about %dMiB/sec (ETA: %s)", (double) 0, (double) etfs.getLogicalTapeInfo().getTableOfContentSection().getFileEntries().stream().map(FileEntry::getFileSize).reduce(Long::sum).get() / Math.pow(1024, 3), (double) 0, Math.round(0), "N/A"));
                }

                @Override
                public void onStartWriteFile(Path target, long fileSize) {
                }

                @Override
                public void onEndWriteFile(Path target, long fileSize) {
                    long timeTaken = System.currentTimeMillis() - startTime[0];
                    double mbps = (fileSize / Math.pow(1024, 2)) / (timeTaken / 1000);
                    //if (fileIndex[0] % 500 == 0)
                    //    System.out.println("Ending write for \"" + target + "\" (File " + fileIndex[0] + " of " + etfs.getTableOfContentSection().getFileEntries().size() + ") in " + Math.round(timeTaken / 1000) + " seconds with " + Math.round(mbps) + " MB/s");
                    fileIndex[0]++;
                }

                @Override
                public MultitapeAction multitapeTapeChangeNeeded(String volumeLabel, UUID volumeId, int currentTapeIndex, int nextTapeIndex) {
                    try {
                        if (tapeDrive instanceof ExtendedTapeDrive) {
                            System.out.println("Tape full. Ejecting");
                            ((ExtendedTapeDrive) tapeDrive).ejectTape();
                        } else {
                            System.out.println("Tape full. Please eject and insert new one!");
                        }

                        System.out.println("Waiting for ejection to finish");
                        while (tapeDrive.isMediaPresent()) {
                            Thread.sleep(1000);
                        }

                        System.out.println("Please insert next tape");
                        while (!tapeDrive.isMediaPresent()) {
                            Thread.sleep(1000);
                        }

                        startTime[0] = System.currentTimeMillis();

                        return MultitapeAction.TAPE_CHANGED;
                    } catch (Exception e) {
                        return MultitapeAction.ABORT;
                    }
                }
            });
        } catch (Exception e) {
            LOGGER.error("Error while serialising data to tape:", e);
            return CLI_RESULT_ERROR_CODE.DEVICE_OPERATION_ERROR.getIntCode();
        }

        print(null);
        System.out.println("Done");

        return CLI_RESULT_ERROR_CODE.NO_ERROR.getIntCode();
    }

    protected static boolean applyMatchers(Path p, PathMatcher[] matchers, boolean invert) {
        if (matchers == null || matchers.length == 0) {
            return true;
        }

        if (invert) {
            for (PathMatcher m : matchers) {
                if (m.matches(p)) {
                    return false;
                }
            }

            return true;
        } else {
            for (PathMatcher m : matchers) {
                if (m.matches(p)) {
                    return true;
                }
            }

            return false;
        }
    }

    protected static PathMatcher[] parseMatchers(String[] pattern, String syntax) {
        PathMatcher[] matchers = new PathMatcher[pattern.length];
        for (int i = 0; i < matchers.length; i++) {
            matchers[i] = FileSystems.getDefault().getPathMatcher(syntax+":"+pattern[i]);
        }

        return matchers;
    }

    private void print(String msg) {
        if (msg == null) {
            lastOut = "";
            System.out.println();
            return;
        }

        int minSize = lastOut.length();
        StringBuilder msgBuilder = new StringBuilder(msg);
        while (msgBuilder.length() < minSize) {
            msgBuilder.append(' ');
        }
        msg = msgBuilder.toString();

        System.out.print(msg+'\r');
        lastOut = msg;
    }
}
